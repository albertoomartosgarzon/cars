<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>Ejemplo de uso de servicio web</title>
</head>
<body>
<?php

try {
    $client = new SoapClient(null, array(
            'uri' => 'https://soap-automoviles.herokuapp.com/',
            'location' => 'https://soap-automoviles.herokuapp.com/service-automoviles-auth.php',
            'trace' => 1
        )
    );
    $auth_params = new stdClass();
    $auth_params->username = 'ies';
    $auth_params->password = 'daw';


    $header_params = new SoapVar($auth_params, SOAP_ENC_OBJECT);
    $authentication = $client->authenticate($header_params);

    $marcas = $client->ObtenerMarcas();

} catch (Exception $e) {
    echo($client->__getLastResponse());
    echo PHP_EOL;
    echo($client->__getLastRequest());
}

?>
